import Foundation
import UIKit

class ActivityDetailsAdapter: NSObject {
    
    enum SectionType {
        case image(_ url: String)
        case info(_ desc: String, _ address: String)
        case product(_ product: Product)
    }
    
    var sections: [SectionType] = []
    
    func bind(to tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ActivityImageTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ActivityImageTableViewCell.identifier)
        tableView.register(UINib(nibName: "ActivityInfosTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ActivityInfosTableViewCell.identifier)
        tableView.register(UINib(nibName: "ActivityProductTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: ActivityProductsTableViewCell.identifier)
    }
    
    func setData(_ shop: Shop, to tableView: UITableView) {
        sections = [.image(shop.image ?? ""), .info(shop.shopDescription ?? "", shop.address ?? "")]
        
        if let products = shop.products {
            sections += products.map({ product in
                    .product(product)
            })
        }
        
        tableView.reloadData()
    }
}


extension ActivityDetailsAdapter: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension ActivityDetailsAdapter: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard self.sections.count > 0 else { fatalError() }
        
        let section = sections[indexPath.section]
        
        switch section {
        case .image(let url):
            if let cell = tableView.dequeueReusableCell(withIdentifier: ActivityImageTableViewCell.identifier, for: indexPath) as? ActivityImageTableViewCell{
                cell.setLabels(url: url)
                return cell
            }
            break
        case .info(let desc, let address):
            if let cell = tableView.dequeueReusableCell(withIdentifier: ActivityInfosTableViewCell.identifier, for: indexPath) as? ActivityInfosTableViewCell {
                cell.setLabels(
                    desc: desc,
                    address: address)
                return cell
            }
            break
        case .product(let product):
            if let cell = tableView.dequeueReusableCell(withIdentifier: ActivityProductsTableViewCell.identifier, for: indexPath) as? ActivityProductsTableViewCell {
                cell.setProductInfos(
                    name: product.name ?? "",
                    price: product.price ?? 0.0,
                    imgUrl: product.productImage ?? "",
                    desc: product.productDescription ?? "")
                return cell
            }
            break
        }
        
        fatalError()
    }
}
