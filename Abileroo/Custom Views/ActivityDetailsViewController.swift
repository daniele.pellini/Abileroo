import Foundation
import UIKit

class ActivityDetailsViewController: UIViewController {
    
    let store = DataManager.shared
    var activity: Shop?
    var isFav: Bool = false
    
    @IBOutlet weak var activityTableView: UITableView!
    
    private lazy var adapter: ActivityDetailsAdapter = {
        ActivityDetailsAdapter()
    }()
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
    }
    
    init(activity: Shop? = nil, isFav: Bool = false) {
        self.activity = activity
        self.isFav = isFav
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = activity?.name
        setBtnImg()
        
        self.adapter.bind(to: activityTableView)
        
        if let shop = self.activity {
            self.adapter.setData(shop, to: activityTableView)
        }
    }
    
    
    @IBAction func favButtonTapAction(_ sender: Any) {
        if !isFav{
            isFav = true
            setBtnImg()
            store.addFavourites(shop: activity!)
        } else {
            isFav = false
            setBtnImg()
            store.removeFavourite(shop: activity!)
        }
    }
    
    func setBtnImg() {
        let imgName = isFav ? "bookmark.fill" : "bookmark"
        let btnImg = UIImage(systemName: imgName)
        self.navigationItem.rightBarButtonItem?.image = btnImg
    }
}
