import Foundation
import UIKit

class AppNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // funzione colore status bar
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
        // contenuto variabile a seconda del tema
        //self.traitCollection.userInterfaceStyle == .dark ? .lightContent : .darkContent
    }
    
    // override per mantenere il cambio dello style generale
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

    }
}
