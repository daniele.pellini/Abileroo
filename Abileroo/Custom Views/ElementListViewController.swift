import UIKit

class ElementListViewController: UIViewController {
    
    let apiCaller = ApiCaller()
    let stored = DataManager.shared
    
    @IBOutlet weak var elementsSearchBar: UISearchBar!
    @IBOutlet weak var elementsTableView: UITableView!
    
    var activities: [Shop]?
    var favShops: [Shop]?
    
    var filteredActivities = [Shop]()
    
    var showFavs: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        elementsTableView.delegate = self
        elementsTableView.dataSource = self
        elementsSearchBar.delegate = self
        elementsSearchBar.showsCancelButton = false
        
        fixNavBar()
        registerCell()
        setBtnImg()
        loadShops()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadShops()
    }
        
    @IBAction func showFavourites(_ sender: Any) {
        if !showFavs{
            showFavs = true
            setBtnImg()
            filteredActivities = favShops ?? []
            self.elementsTableView.reloadData()
        } else {
            showFavs = false
            setBtnImg()
            filteredActivities = activities ?? []
            self.elementsTableView.reloadData()
        }
    }
    
    func loadShops() {
        apiCaller.getShops { shops, error in
            if let shops = shops {
                self.activities = shops
                self.filteredActivities = shops
                self.elementsTableView.reloadData()
            }
        }
        
        if let data = stored.getFavoritesList() {
            favShops = data
        }
        
    }
    
    func setBtnImg() {
        let imgName = showFavs ? "star.fill" : "star"
        let btnImg = UIImage(systemName: imgName)
        self.navigationItem.rightBarButtonItem?.image = btnImg
    }
    
    func fixNavBar() {
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            appearance.backgroundColor = UIColor(hexString: "B76962")
            navigationController?.navigationBar.standardAppearance = appearance
            navigationController?.navigationBar.scrollEdgeAppearance = navigationController?.navigationBar.standardAppearance
        }
    }
}

extension ElementListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredActivities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if let cell = tableView.dequeueReusableCell(
            withIdentifier: CustomTableViewCell.identifier,
            for: indexPath) as? CustomTableViewCell {
            
            cell.setCellInfos(
                url: filteredActivities[indexPath.row].image ?? "",
                name: filteredActivities[indexPath.row].name ?? "",
                address: filteredActivities[indexPath.row].address ?? "")
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "ActivityDetails", sender: filteredActivities[indexPath.row])
    }
    
    func registerCell(){
        elementsTableView.register(UINib(nibName: "CustomTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: CustomTableViewCell.identifier)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let myActivity = sender as? Shop else { return }

        if let destination = segue.destination as? ActivityDetailsViewController {
            destination.activity = myActivity
            if stored.favShopsList.contains(where: { $0.id == myActivity.id }) {
                destination.isFav = true
            } else {
                destination.isFav = false
            }
        }
    }
}

extension ElementListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText != "" {
            
            filteredActivities.removeAll()
            
            if showFavs {
                
                favShops?.forEach({ shop in
                    if shop.name != "" && shop.name!.uppercased().contains(searchText.uppercased()) {
                        filteredActivities.append(shop)
                    }
                })
                
            } else {
                
                activities?.forEach({ shop in
                    if shop.name != "" && shop.name!.uppercased().contains(searchText.uppercased()) {
                        filteredActivities.append(shop)
                    }
                })
                
            }
            
        } else {
            filteredActivities = showFavs ? (favShops ?? []) : (activities ?? [])
        }
        
        self.elementsTableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        elementsSearchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        elementsSearchBar.setShowsCancelButton(false, animated: true)
        if showFavs {
            filteredActivities = favShops ?? []
            elementsTableView.reloadData()
        } else {
            filteredActivities = activities ?? []
            elementsTableView.reloadData()
        }
        elementsSearchBar.resignFirstResponder()
    }
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}
