import UIKit

class ActivityProductsTableViewCell: UITableViewCell {

    static let identifier = "ActivityProductsCell"
    
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productDesc: UILabel!
    
    func setProductInfos(name: String, price: Double, imgUrl: String, desc: String){
        productName.text = name
        productPrice.text = "\(price) €"
        productImg.loadImage(url: imgUrl)
        productDesc.text = desc
    }
}
