import UIKit

class ActivityImageTableViewCell: UITableViewCell {
    
    static let identifier = "ActivityImageCell"
    
    @IBOutlet weak var activityImage: UIImageView!
    
    func setLabels(url: String) {
        activityImage.loadImage(url: url)
    }
    
}
