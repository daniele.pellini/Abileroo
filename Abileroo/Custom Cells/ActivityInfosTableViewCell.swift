import UIKit

class ActivityInfosTableViewCell: UITableViewCell {
    
    static let identifier = "ActivityInfosCell"
    
    @IBOutlet weak var activityDescription: UITextView!
    @IBOutlet weak var activityAddress: UITextView!

    func setLabels(desc: String, address: String) {
        activityDescription.text = desc
        activityAddress.text = address
    }

}
