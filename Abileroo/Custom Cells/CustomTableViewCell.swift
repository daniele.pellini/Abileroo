import UIKit

class CustomTableViewCell: UITableViewCell {
    
    static let identifier = "CustomTableViewCell"
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellAddress: UILabel!
    
    func setCellInfos(url: String, name: String, address: String){
        cellImage.loadImage(url: url)
        cellName.text = name
        cellAddress.text = address
    }
}

extension UIImageView {
    func loadImage(url: String) {
        DispatchQueue.global(qos: .background).async {
            guard let url = URL(string: url) else { return }
            
            if let data = try? Data(contentsOf: url) {
                DispatchQueue.main.async {
                    self.image = UIImage(data: data)
                }
            }
        }
    }
}

extension UITableViewCell {
    static var reusableIdentifier: String {
        String(describing: Self.self)
    }
}
