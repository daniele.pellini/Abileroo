import Foundation
import Alamofire

class ApiCaller {
    
    @discardableResult
    func executeGetCall<T: Codable>(api: APIREQUEST, responseType: T.Type, completion: @escaping (Result<T, Error>) -> Void) -> Request {
        let request = AF.request(api).responseData { response in
            switch response.result {
            case .success(let data):
                
                if let result = try? JSONDecoder().decode(T.self, from: data) {
                    DispatchQueue.main.async {
                        completion(.success(result))
                    }
                    return
                }
                
                DispatchQueue.main.async {
                    let error = NSError(domain: "", code: 101, userInfo: [NSLocalizedDescriptionKey: "Errore metodo GET"])
                    completion(.failure(error))
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
                break
            }
        }
        return request
    }
    
    
    func getShops(completion: @escaping ([Shop]?, Error?) -> Void) {

       executeGetCall(api: APIREQUEST.shops, responseType: [Shop].self) { result in
            switch result {
            case .success(let shops):
                completion(shops, nil)
                break
            case .failure(let error):
                completion(nil, error)
                print(error)
                break
            }
        }
    }
    
    
    
    
    func getActivities(api: APIREQUEST, completion: @escaping(Result<[Shop], Error>) -> Void) {
        
        AF.request(api).responseData { response in
            switch response.result {
            case .success(let data):
                
                if let result = try? JSONDecoder().decode([Shop].self, from: data) {
//                    DispatchQueue.main.async {
//                        completion(.success(result))
//                    }
                    
                    
                    completion(.success(result))
                    return
                }
                
//                DispatchQueue.main.async {
//                    let error = NSError(domain: "", code: 101, userInfo: [NSLocalizedDescriptionKey: "Errore get activities"])
//                    completion(.failure(error))
//                }
                
                let error = NSError(domain: "", code: 101, userInfo: [NSLocalizedDescriptionKey: "Errore get activities"])
                completion(.failure(error))
                break
                
            case .failure(let error):
//                DispatchQueue.main.async {
//                    completion(.failure(error))
//                }
                
                completion(.failure(error))
                break
            }
        }.resume()

    }
    
}

