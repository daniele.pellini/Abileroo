import Foundation
import Alamofire

enum APIREQUEST {
    case shops
    case shopsListFromAtoZ
    case shopsListFromZtoA
    case findShopListByName(String)
    case shopDetails(Int)
    case newShop
    
    case productById(Int)
    case productsByShopName(String)
    case productsByShopNameFromAtoZ(String)
    case productsByShopNameFromZtoA(String)
    case productsListByShopId(Int)
    case newProductForShop(Int)
    
    case ordersListByEmail(String)
    case orderById(Int)
    case ordersList
    case newOrder
}

extension APIREQUEST {
    var method: HTTPMethod {
        switch self {
        case .shops: return .get
        case .shopsListFromAtoZ: return .get
        case .shopsListFromZtoA: return .get
        case .findShopListByName: return .get
        case .shopDetails: return .get
        case .newShop: return .post
            
        case .productById: return .get
        case .productsByShopName: return .get
        case .productsByShopNameFromAtoZ: return .get
        case .productsByShopNameFromZtoA: return .get
        case .productsListByShopId: return .get
        case .newProductForShop: return .post
            
        case .ordersListByEmail: return .get
        case .orderById: return .get
        case .ordersList: return .get
        case .newOrder: return .post
        }
    }
}

extension APIREQUEST {
    var baseURL: String {
        return "https://enrobax.pythonanywhere.com/api/"
    }
    
    var path: String {
        switch self {
        case .shops: return "shops/"
        case .shopsListFromAtoZ: return "shops/?ordering=name"
        case .shopsListFromZtoA: return "shops/?ordering=-name"
        case .findShopListByName(let name): return "shops/?search=\(name)"
        case .shopDetails(let id): return "shop/\(id)/"
        case .newShop: return "shops/"
            
        case .productById(let id):  return "product/\(id)"
        case .productsByShopName(let name): return "product/s_name/\(name)/all"
        case .productsByShopNameFromAtoZ(let name): return "product/s_name/\(name)/a-z"
        case .productsByShopNameFromZtoA(let name): return "product/s_name/\(name)/z-a"
        case .productsListByShopId(let id): return "products/shop_id/\(id)/"
        case .newProductForShop(let id): return "products/shop_id/\(id)/"
            
        case .ordersListByEmail(let email): return "orders/\(email)"
        case .orderById(let id): return "order/\(id)"
        case .ordersList: return "orders/"
        case .newOrder: return "orders/"
        }
    }
}

extension APIREQUEST: URLRequestConvertible {
    func asURLRequest() throws -> URLRequest {
        guard let url = URL(string: "\(self.baseURL)\(self.path)") else { fatalError() }
        let method = self.method
        var request = URLRequest(url: url)
        request.method = method
        return request
    }
}
