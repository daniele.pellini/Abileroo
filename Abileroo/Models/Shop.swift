struct Shop: Codable {
    let id: Int?
    let name, address, shopDescription, image: String?
    let products: [Product]?

    enum CodingKeys: String, CodingKey {
        case id, name, address
        case shopDescription = "description"
        case image, products
    }
}

struct Product: Codable {
    let id, shop: Int?
    let name, productImage, productDescription: String?
    let price: Double?
    let availableAmount: Int?

    enum CodingKeys: String, CodingKey {
        case id, shop, name
        case productImage = "product_image"
        case productDescription = "description"
        case price
        case availableAmount = "available_amount"
    }
}
