import Foundation

class DataManager {
    
    static let shared = DataManager()
    
    let defaults = UserDefaults.standard
    let decoder = JSONDecoder()
    let encoder = JSONEncoder()
    let FAV_SHOPS_LIST_KEY = "favShopsListKey"
    let FAV_SHOP_KEY = "favShopKey"
    
    var favShopsList = [Shop]()
    
    func getFavoritesList() -> [Shop]? {
        if let data = defaults.data(forKey: FAV_SHOPS_LIST_KEY) {
            do {
                favShopsList = try decoder.decode([Shop].self, from: data)
                return favShopsList
            } catch {
                print("Errore GET shop preferiti")
            }
        }
        
        return nil
    }
    
    func addFavourites(shop: Shop) {
        do {
            favShopsList = getFavoritesList() ?? []
            favShopsList.append(shop)
            let data = try encoder.encode(favShopsList)
            defaults.set(data, forKey: FAV_SHOPS_LIST_KEY)
        } catch {
            print("Errore ADD in lista preferiti")
        }
    }
    
    func removeFavourite(shop: Shop) {
        do {
            favShopsList = getFavoritesList() ?? []
            favShopsList.removeAll(where: { $0.id == shop.id })
            let data = try encoder.encode(favShopsList)
            defaults.set(data, forKey: FAV_SHOPS_LIST_KEY)
        } catch {
            print("Errore DELETE dalla lista")
        }
    }
}
