import Foundation

struct Order: Codable {
    var attività: Shop
    var prodotti: [Product]
    var momento_acquisto: String
    var indirizzo_consegna: String
    var email: String
}
